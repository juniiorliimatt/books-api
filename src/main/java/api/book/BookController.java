package api.book;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/book")
@Slf4j
public class BookController implements Serializable{
  private static final long serialVersionUID = 1L;

  private final BookService service;

  @GetMapping("/find/all")
  public ResponseEntity<List<Book>> findAll(){
	return ResponseEntity.ok(service.findAll());
  }

  @GetMapping("find/id/{id}")
  public ResponseEntity<Optional<Book>> findById(@PathVariable("id") long id){
	log.info("Teste");
	return ResponseEntity.ok(service.findById(id));
  }

  @PostMapping("/save")
  public ResponseEntity<Book> save(@RequestBody Book book){
	URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
										 .path("find/id/{id}")
										 .buildAndExpand(book.getId())
										 .toUri();
	return ResponseEntity.created(uri)
						 .body(service.save(book));
  }

  @PutMapping("/update")
  public ResponseEntity<Book> update(@RequestBody Book book){
	return ResponseEntity.ok(service.save(book));
  }

  @DeleteMapping("/delete/id/{id}")
  public ResponseEntity<Book> delete(@PathVariable("id") long id){
	service.deleteById(id);
	return ResponseEntity.ok()
						 .build();
  }

}
